# SMART BANK

## Introduction
Smart bank allow bank agent to create new account for existing customers, doing transactions and have more informations about every customer.
Smart bank is composed of two parts. The front part and the back part.

## Running the application
To run the application make sure you have this environment
### backend
* Java 8
* maven 3
* git
* gitlab account (for cloning)
### frontend
* git
* gitlab account(for cloning)
* angular 14
* npm 8 
* node 16 

To run this application flow this these steps.
### Running backend service
Installing backend project: "mvn install".   
Make sure first 8080 port is available.  
Run the application.
If running, you can access your in memory h2  database via http://localhost:8080/h2-console.
With username: sa and without password(see application.properties to change).
Some customers are already put(from data.sql) and visible from frontend.

### Running frontend
Frontend application is based on angular. To run it type "ng serve -o" from cli at project root directory.

# UI Content
The UI is divided into tree section.
## Section 1
Show all customers that don't have yet an account
## Section 2
Allow creation of an account for customers
## Section 3
Show customers that have already an account and allow getting more information about each customer if needed.
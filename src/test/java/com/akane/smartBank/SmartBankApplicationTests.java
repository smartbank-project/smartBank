package com.akane.smartBank;

import com.akane.smartBank.controller.CostumerController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SmartBankApplicationTests {

	@Autowired
	CostumerController costumerController;
	@Test
	void nationsLength() {
		Integer customersLength = costumerController.findAllCustomers().size();
		Assertions.assertEquals(8, customersLength);
	}

}

package com.akane.smartBank.controller;

import com.akane.smartBank.controller.mapper.CustomerMapper;
import com.akane.smartBank.entity.Account;
import com.akane.smartBank.entity.Customer;
import com.akane.smartBank.entity.Transaction;
import com.akane.smartBank.entity.dto.AccountCreationDto;
import com.akane.smartBank.entity.dto.CustomerInformationsDto;
import com.akane.smartBank.entity.enums.TransactionType;
import com.akane.smartBank.helper.Route;
import com.akane.smartBank.service.interfaces.AccountService;
import com.akane.smartBank.service.interfaces.CustomerService;
import com.akane.smartBank.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
public class CostumerController {
    private CustomerService customerService;
    private CustomerMapper customerMapper;
    private AccountService accountService;
    private TransactionService transactionService;
    @Autowired
    public CostumerController(CustomerService customerService, CustomerMapper customerMapper, AccountService accountService,
                              TransactionService transactionService){
        this.customerService = customerService;
        this.customerMapper = customerMapper;
        this.accountService = accountService;
        this.transactionService = transactionService;
    }

    /**
     * Return all customers who don't having account
     * @return customers
     */
    @GetMapping(Route.ALL_CUSTOMER)
    public List<Customer> findAllCustomers(){return customerService.findAllCustomers().stream().filter((customer -> customer.getAccount() == null)).collect(Collectors.toList());
    }

    /**
     * Return all customers having account
     * @return customers
     */
    @GetMapping(Route.ALL_CUSTOMER_WITH_ACCOUNT)
    public List<Customer> findAllCustomersWithAccount(){return customerService.findAllCustomers().stream().filter((customer -> customer.getAccount() != null)).collect(Collectors.toList());
    }

    /**
     * Create new account
     * @param accountCreationDto
     * @return
     */
    @PostMapping(Route.CREATE_ACCOUNT)
    public Customer createAccount(@RequestBody AccountCreationDto accountCreationDto){
        Customer customer = customerService.findByCustomerId(accountCreationDto.getCustomerId());
        Account account = new Account();
        if(accountCreationDto.getInitialCredit() != 0) {
            account.setBalance(accountCreationDto.getInitialCredit());
            Transaction transaction = new Transaction(TransactionType.CREDIT, accountCreationDto.getInitialCredit());
            transaction.setTransactionType(TransactionType.CREDIT);
            transactionService.updateAccount(transaction);
            account.setTransactions(Arrays.asList(transaction));
        }else {
            account.setBalance(0.0);
        }
        accountService.updateAccount(account);
        customer.setAccount(account);
        return customerService.updateCustomer(customer);
    }

    /**
     * Return information about a customer
     * @param id
     * @return customerInformationsDto
     */
    @GetMapping(Route.GET_USER_INFO)
    public CustomerInformationsDto getCustomerInfo(@PathVariable Long id){
       return this.customerMapper.toCustomerInformationsDto(this.customerService.findCustomerById(id));
    }
}

package com.akane.smartBank.controller.mapper;

import com.akane.smartBank.entity.Customer;
import com.akane.smartBank.entity.Transaction;
import com.akane.smartBank.entity.dto.CustomerInformationsDto;
import com.akane.smartBank.entity.dto.TransactionDto;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class CustomerMapper {

    public CustomerInformationsDto toCustomerInformationsDto(Customer customer){
        return new CustomerInformationsDto(customer.getName(), customer.getSurname(), customer.getAccount().getBalance(),
                customer.getAccount().getTransactions().stream().map(trans -> new TransactionDto(trans.getTransactionType(), trans.getAmount(), trans.getTransactionTime())).collect(Collectors.toList()));
    }

}

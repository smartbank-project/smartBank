package com.akane.smartBank.entity.enums;

public enum TransactionType {
    CREDIT,
    DEBIT
}

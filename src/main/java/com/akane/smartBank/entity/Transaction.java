package com.akane.smartBank.entity;

import com.akane.smartBank.entity.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transaction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;
    @Column(nullable = false)
    private Double amount;
    @Column(nullable = false)
    private LocalDateTime transactionTime = LocalDateTime.now();
    public Transaction(TransactionType transactionType, Double amount){
        this.transactionType = transactionType;
        this.amount = amount;
        this.transactionTime = LocalDateTime.now();
    }
}

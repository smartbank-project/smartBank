package com.akane.smartBank.entity.dto;

import com.akane.smartBank.entity.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
@AllArgsConstructor
@Data
public class CustomerInformationsDto {
    private String name;
    private String surname;
    private double balance;
    private List<TransactionDto> transactions;

}
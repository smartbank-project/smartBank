package com.akane.smartBank.entity.dto;

import lombok.Data;

@Data
public class AccountCreationDto {
    private String customerId;
    private double initialCredit;
}

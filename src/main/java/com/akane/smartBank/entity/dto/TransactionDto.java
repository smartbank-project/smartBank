package com.akane.smartBank.entity.dto;

import com.akane.smartBank.entity.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
@AllArgsConstructor
@Data
public class TransactionDto implements Serializable {
        private TransactionType transactionType;
        private double amount;
        private LocalDateTime transactionTime;
}

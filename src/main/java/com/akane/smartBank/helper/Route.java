package com.akane.smartBank.helper;

public final class Route {
    public static final String ALL_CUSTOMER = "/customers";
    public static final String ALL_CUSTOMER_WITH_ACCOUNT = "/customers/active";
    public static final String CREATE_ACCOUNT = "/new-account";
    public static final String GET_USER_INFO = "/customer/{id}";
}

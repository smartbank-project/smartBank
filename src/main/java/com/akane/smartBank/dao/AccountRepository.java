package com.akane.smartBank.dao;

import com.akane.smartBank.entity.Account;
import com.akane.smartBank.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}

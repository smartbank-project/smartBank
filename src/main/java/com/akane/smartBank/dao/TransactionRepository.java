package com.akane.smartBank.dao;

import com.akane.smartBank.entity.Customer;
import com.akane.smartBank.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
}

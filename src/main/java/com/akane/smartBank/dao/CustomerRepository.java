package com.akane.smartBank.dao;

import com.akane.smartBank.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    public Customer findByCustomerId(String customerId);
}

package com.akane.smartBank.service.interfaces;

import com.akane.smartBank.entity.Customer;

import java.util.List;

public interface CustomerService {
    public List<Customer> findAllCustomers();
    public Customer findCustomerById(Long id);
    public Customer findByCustomerId(String customerId);
    public Customer updateCustomer(Customer customer);
}

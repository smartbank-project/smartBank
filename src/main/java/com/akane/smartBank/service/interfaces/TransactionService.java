package com.akane.smartBank.service.interfaces;

import com.akane.smartBank.entity.Transaction;

import java.util.List;

public interface TransactionService {
    public List<Transaction> findAllTransaction();
    public Transaction findTransactionById(Long id);
    public Transaction updateAccount(Transaction transaction);
}

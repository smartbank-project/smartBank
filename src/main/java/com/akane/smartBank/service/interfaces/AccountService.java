package com.akane.smartBank.service.interfaces;

import com.akane.smartBank.entity.Account;

import java.util.List;

public interface AccountService {
    public List<Account> findAllAccount();
    public Account findAccountById(Long id);
    public Account updateAccount(Account account);
}

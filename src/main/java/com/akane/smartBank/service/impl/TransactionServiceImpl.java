package com.akane.smartBank.service.impl;

import com.akane.smartBank.dao.TransactionRepository;
import com.akane.smartBank.entity.Transaction;
import com.akane.smartBank.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;
    @Override
    public List<Transaction> findAllTransaction() {
        return transactionRepository.findAll();
    }

    @Override
    public Transaction findTransactionById(Long id) {
        return transactionRepository.findById(id).get();
    }

    @Override
    public Transaction updateAccount(Transaction transaction) {
        return transactionRepository.saveAndFlush(transaction);
    }
}

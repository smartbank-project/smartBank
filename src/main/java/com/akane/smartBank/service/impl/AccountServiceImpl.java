package com.akane.smartBank.service.impl;

import com.akane.smartBank.dao.AccountRepository;
import com.akane.smartBank.entity.Account;
import com.akane.smartBank.service.interfaces.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;
    @Override
    public List<Account> findAllAccount() {
        return accountRepository.findAll();
    }

    @Override
    public Account findAccountById(Long id) {
        return accountRepository.findById(id).get();
    }

    @Override
    public Account updateAccount(Account account) {
        return accountRepository.saveAndFlush(account);
    }
}

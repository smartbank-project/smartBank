INSERT INTO CUSTOMER (id, customer_id, name, surname) VALUES (1, '100 200 300 400', 'Amadou', 'KANE');
INSERT INTO CUSTOMER (id, customer_id, name, surname) VALUES (2, '200 200 300 100', 'Joseph', 'DIOP');
INSERT INTO CUSTOMER (id, customer_id, name, surname) VALUES (3, '300 200 300 200', 'Jean', 'MUSK');
INSERT INTO CUSTOMER (id, customer_id, name, surname) VALUES (4, '400 200 300 300', 'Bill', 'ZUCK');
INSERT INTO CUSTOMER (id, customer_id, name, surname) VALUES (5, '500 200 300 500', 'Ibrahima', 'BA');
INSERT INTO CUSTOMER (id, customer_id, name, surname) VALUES (6, '600 200 300 600', 'Samba', 'DUBOIS');
INSERT INTO CUSTOMER (id, customer_id, name, surname) VALUES (7, '700 200 300 700', 'Eric', 'DRAGI');
INSERT INTO CUSTOMER (id, customer_id, name, surname) VALUES (8, '800 200 300 800', 'Simon', 'SARR');